package br.com.felix.zuulserver

import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Order(2)
@Component
class CorrectHeadersFilter : ZuulFilter() {

    override fun filterType(): String {
        return "pre"
    }

    override fun filterOrder(): Int {
        return 10000
    }

    override fun shouldFilter(): Boolean {
        return true
    }

    override fun run(): Any? {
        val currentContext = RequestContext.getCurrentContext()

        currentContext.addZuulRequestHeader("host", currentContext.request.getHeader("host"))

        currentContext.zuulRequestHeaders.remove("x-forwarded-prefix")

        (if (currentContext["ignoredHeaders"] != null) currentContext["ignoredHeaders"] as MutableSet<*> else null)?.clear()

        return null
    }


}